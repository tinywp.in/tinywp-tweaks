<?php

/**
 * Plugin Name: TinyWP Tweaks
 * Plugin URI: https://gitlab.com/tinywp.in/tinywp-tweaks
 * Version: 1.0
 * Author: Pothi Kalimuthu  
 * Author URI: https://www.tinywp.com/
 * Description: Tiny WordPress Tweaks for tinywp.in!
 */

/**
 * Do not activate any modules by default
 * Ref: https://developer.jetpack.com/hooks/jetpack_get_default_modules/ (under 'notes')
 * Props: Mark Jaquith
 */

add_filter( 'jetpack_get_default_modules', '__return_empty_array' );

